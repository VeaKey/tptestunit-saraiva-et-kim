package project;

import java.util.Date;
import java.util.HashMap;

public class Systeme {

	HashMap<String, Projet> listProjet;

	public void addProjet(String nom, int nbJours){
		listProjet.put(nom,new Projet(nom,nbJours));
	}
	
	public void addProjet(String nom, int nbJours, Date date){
		listProjet.put(nom,new Projet(nom,nbJours, date));
	}
	
	public Systeme() {
		listProjet = new HashMap<String, Projet>();
	}
	
	public Projet getProjetWithKey(String toFind){
		return listProjet.get(toFind);
	}
	
	public static void main (String[] args){
		new Systeme();
	}
	
}
