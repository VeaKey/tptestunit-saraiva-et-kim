package project;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.lang.*;

public class Projet {

	String nom;
	String productOwner;
	String etat; // opened closed
	int budget;
	Date date;
	
	HashMap<String, UserStory> listUserStory;
	
	public Projet(String nom, int nbJours) throws NullPointerException{
		
		if ("".equals(nom) || nbJours == 0 )
			throw new NullPointerException();
		this.nom = nom;
		this.budget = nbJours;
		productOwner = "default";
		etat = Constantes.Projet_OPEN;
		date = new Date();
		listUserStory = new HashMap<String, UserStory>();
	}
	
	public Projet(String nom, int nbJours, Date date) throws NullPointerException{
		
		if ("".equals(nom) || nbJours == 0 )
			throw new NullPointerException();
		this.nom = nom;
		this.budget = nbJours;
		productOwner = "default";
		etat = Constantes.Projet_OPEN;
		this.date = date;
		listUserStory = new HashMap<String, UserStory>();
	}
	
	public String getFormatedDate(){
		return DateFormat.getDateInstance(DateFormat.MEDIUM).format(this.date);
	}
	
	//TODO ajouter un test � cette fonction
	public String getEtat(){
		boolean isClosed = !checkUserStoriesOpen();
		etat = isClosed ? Constantes.Projet_CLOSED : etat;
		return etat;
	}
	
	/*
	 * Vrai si tous les UserStory sont closed
	 */
	private boolean checkUserStoriesOpen() {
		for (UserStory userStory : listUserStory.values()) {
			if (!userStory.getEtat().equals(Constantes.UserStory_CLOSED)){
				return false;
			}
		}
		return true;
	}

	public UserStory getUserStoryWithKey(String toFind){
		return listUserStory.get(toFind);
	}

	public void addUserStory(String nom, int charge) {
		listUserStory.put(nom, new UserStory(nom,charge));		
	}
	
	public void addUserStory(String nom) {
		listUserStory.put(nom, new UserStory(nom));		
	}

	public String getNom() {
		return nom;
	}
	
}
