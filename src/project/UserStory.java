package project;

import java.util.Date;

public class UserStory {

	String nom;
	String etatUS;
	Avancement avancement;
	Integer charge;
	Historique historique;
	
	public UserStory(String nom) {
		this.nom = nom;
		this.charge = null;
		etatUS = Constantes.UserStory_NEW;
		avancement = new Avancement();
		historique= new Historique();
	}
	
	public UserStory(String nom, int charge) {
		this.nom = nom;
		this.charge = charge;
		etatUS = Constantes.UserStory_PLANIFIED;
		avancement = new Avancement();
		historique= new Historique();
	}
	
	public String getEtat(){
		if(charge != null)
			if(charge == 0 ){
				etatUS = Constantes.UserStory_CLOSED;
			}
		return this.etatUS;
	}

	public void setTempsRestant(int newCharge) {
		charge = newCharge;
	}

	public Integer getTempsRestant() {
		return charge;
	}

	public void changeRemainingDays(int days, Date date) {
		charge = days;
		historique.addNewReste(date, charge);
	}
}
