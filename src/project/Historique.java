package project;

import java.util.Date;
import java.util.HashMap;

public class Historique {

	HashMap<Integer, ResteAFaire> listResteAFaire;

	public Historique(){
		listResteAFaire = new HashMap<Integer, ResteAFaire>();
	}
	
	public void addNewReste(Date date, Integer charge) {
		listResteAFaire.put(listResteAFaire.size() + 1, new ResteAFaire(date, charge));
	}
	
}
