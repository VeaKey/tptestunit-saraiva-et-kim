package test;

import static org.junit.Assert.*;

import java.util.Date;


import org.junit.Before;
import org.junit.Test;

import project.Constantes;
import project.Projet;
import project.Systeme;
import project.UserStory;

public class ProjectScrumTest {

	Systeme s;
	@Before
	public void Setup(){
		s = new Systeme();
	}
	
	@Test(expected = NullPointerException.class)
	public void testAjoutProjetNonNullNonZero(){
		new Projet("", 0);
	}
	@Test(expected = NullPointerException.class)
	public void testAjoutProjetNonNull(){
		new Projet("", 1);
	}
	@Test(expected = NullPointerException.class)
	public void testAjoutProjetNonZero(){
		new Projet("test", 0);
	}
	
	@Test//(expected = NullPointerException.class)
	public void testCreationUserStoryChargeNul(){
		UserStory us = new UserStory("je n'ai pas de charge");
		assertEquals(Constantes.UserStory_NEW, us.getEtat());
	}
	
	@Test
	public void testAjoutProjet() {
		s.addProjet("refonte de l'interface graphique", 400);
		
		String expected = Constantes.Projet_OPEN;
		String actual = s.getProjetWithKey("refonte de l'interface graphique").getEtat();
		assertEquals(expected, actual);
	}
	
	@Test
	public void testAjoutUserStory() {
		s.addProjet("Refonte de l'interface pour tablettes", 100);
		Projet projet = s.getProjetWithKey("Refonte de l'interface pour tablettes");
		projet.addUserStory("refonte du tunnel d'achat",40);
		
		String expected = Constantes.UserStory_PLANIFIED;
		String actual = projet.getUserStoryWithKey("refonte du tunnel d'achat").getEtat();
		assertEquals(expected, actual);
	}
	
	@Test
	public void testAjoutFonctionnalite(){
		//ajout d'un projet
		s.addProjet("refonte des interfaces graphiques", 400);
		Projet bob = s.getProjetWithKey("refonte des interfaces graphiques");
		
		//ajout d'un user story � bob
		bob.addUserStory("refonte du tunnel d'achat", 10);
		bob.addUserStory("refonte de l'ergonomie en responsive design");
		
		//recup US refonte du tunnel
		UserStory tunnel = bob.getUserStoryWithKey("refonte du tunnel d'achat");
		tunnel.setTempsRestant(0);
		
		//test1
		assertEquals(Constantes.UserStory_CLOSED, s.getProjetWithKey("refonte des interfaces graphiques")
				.getUserStoryWithKey("refonte du tunnel d'achat").getEtat());
		boolean toShow = s.getProjetWithKey("refonte des interfaces graphiques")
				.getUserStoryWithKey("refonte du tunnel d'achat").getTempsRestant() == 0;
		//test2
		assertTrue(toShow);
		//test3
		boolean isProjectClose = s.getProjetWithKey("refonte des interfaces graphiques").getEtat().equals(Constantes.Projet_CLOSED);
		assertTrue(isProjectClose);
	}
	
	@Test
	public void testDernier(){
		s.addProjet("refonte des interfaces graphiques", 400, new Date("09/10/2012"));
		
		Projet p = s.getProjetWithKey("refonte des interfaces graphiques");
		
		p.addUserStory("refonte du tunnel d'achat", 50);
		p.addUserStory("refonte des interfaces graphiques", 100);
		
		UserStory a = p.getUserStoryWithKey("refonte du tunnel d'achat");
			a.changeRemainingDays(45, new Date("11/11/2012"));
		UserStory b = p.getUserStoryWithKey("refonte des interfaces graphiques");
			b.changeRemainingDays(90, new Date("11/12/2012"));
		
		Date c = new Date("11/10/2012");
		Date d = new Date("12/10/2012");
			System.out.println(c.before(d));
		
			
	}

}
