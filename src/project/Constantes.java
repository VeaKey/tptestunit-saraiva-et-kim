package project;

public class Constantes {

	public static String Projet_OPEN = "Opened";
	public static String Projet_CLOSED = "Closed";
	
	public static String UserStory_NEW = "New";
	public static String UserStory_PLANIFIED = "Planified";
	public static String UserStory_CLOSED = "Closed";
	public static String UserStory_REJECTED = "Rejected";
}
